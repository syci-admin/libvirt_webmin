=head1 virt-manager-lib.pl

Base library functions

  foreign_require("virt-manager", "virt-manager-lib.pl");

=cut

BEGIN { push(@INC, ".."); };
use WebminCore;
init_config();
%access = &get_module_acl();

sub exec_virsh {
	my ($dom) = @_;
	my $output = `$config{'virsh_path'} $dom`;
	
	return $output;
}

#Execute the virsh commande with parameters provided
sub get_vncdisplay {
	my ($domain) = @_;
	my $vncout = exec_virsh('vncdisplay '.$domain);
        my $temp = substr $vncout,2,1 ;
       	my $ouput = int($temp);
	return $output;
}
sub get_domainname {
	my ($domain) = @_;
	my %name= get_domain_info_by_name($domain);
        $output = $name{'name'};
	return $output;
}


sub get_domain_info_by_name {
	my ($domain) = @_;
	my $domain_string_info = exec_virsh('dominfo '.$domain);
	my %domain_info = ();
	
	foreach my $l (split(/\n/, $domain_string_info)) {
		if($l =~ /Id:\s+(.*)/) {
			$domain_info{'id'} = $1;
		}
		if($l =~ /Name:\s+(.*)/) {
			$domain_info{'name'} = $1;
		}
		if($l =~ /uuid:\s+(.*)/) {
			$domain_info{'uuid'} = $1;
		}
		if($l =~ /OS Type:\s+(.*)/) {
			$domain_info{'os_type'} = $1;
		}
		if($l =~ /State:\s+(.*)/) {
			$domain_info{'state'} = $1;
		}
		if($l =~ /CPU\(s\):\s+(.*)/) {
			$domain_info{'cpus'} = $1;
		}
		if($l =~ /CPU time:\s+(.*)/) {
			$domain_info{'cup_time'} = $1;
		}
		if($l =~ /Max memory:\s+(.*)/) {
			$domain_info{'max_memory'} = $1;
		}
		if($l =~ /Used memory:\s+(.*)/) {
			$domain_info{'used_memory'} = $1;
		}
	}
	
	return %domain_info;
}

sub get_domain_info_by_id {
	my ($dom) = @_;
	return &get_domain_info_by_name($dom);
}

sub get_domain_info_by_uuid {
	my ($dom) = @_;
	return &get_domain_info_by_name($dom);
}

sub get_domains {
	my $domains_string = exec_virsh("list --all");
	my @domains;
	foreach my $l (split(/\n/, $domains_string)) {
		my %domain;
		if ($l =~ /Id/) {
			next;
		} 
		if ($l =~ /---/) {
			next;
		}
		if ($l =~ /\s+(\S+)\s+(\S+)\s+(.*)$/) {
			$domain{'id'} = $1;
			$domain{'name'} = $2;
			$domain{'status'} = $3;
			my %domain_status = &get_domain_info_by_name($domain{'name'});
			$domain{'cpus'} = $domain_status{'cpus'};
			$domain{'memory'} = &nice_size($domain_status{'used_memory'} * 1024);
			@domains = (@domains, { %domain });
		}
	}
	
	return @domains;
}

sub start_domain {
	my ($dom) = @_;
	return &exec_virsh('start '.$dom);
}

sub destroy_domain {
	my ($dom) = @_;
	return &exec_virsh('destroy '.$dom);
}

sub shutdown_domain {
	my ($dom) = @_;
	return &exec_virsh('shutdown '.$dom);
}

sub suspend_domain {
	my ($dom) = @_;
	return &exec_virsh('suspend '.$dom);
}

sub resume_domain {
	my ($dom) = @_;
	return &exec_virsh('resume '.$dom);
}

1;
