require 'virt-manager-lib.pl';

# acl_security_form(&options)
# Output HTML for editing security options for the virtual manager module
sub acl_security_form
{
print "<tr> <td><b>$text{'acl_reboot'}</b></td> <td>\n";
printf "<input type=radio name=virtreboot value=1 %s> $text{'yes'}\n",
        $_[0]->{'reboot'} ? "checked" : "";
printf "<input type=radio name=virtreboot value=0 %s> $text{'no'}</td>\n",
        $_[0]->{'reboot'} ? "" : "checked";

print "<td><b>$text{'acl_see'}</b></td> <td>\n";
printf "<input type=radio name=virtsee value=1 %s> $text{'yes'}\n",
        $_[0]->{'shutdown'} ? "checked" : "";
printf "<input type=radio name=virtsee value=0 %s> $text{'no'}</td> </tr>\n",
        $_[0]->{'shutdown'} ? "" : "checked";
}

# acl_security_save(&options)
# Parse the form for security options for the init module
sub acl_security_save
{
$_[0]->{'virtreboot'} = $in{'virtreboot'};
$_[0]->{'virtsee'} = $in{'virtsee'};
}
