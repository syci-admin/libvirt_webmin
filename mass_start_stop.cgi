#!/usr/bin/perl

require 'virt-manager-lib.pl';

&ReadParse();

my @sel = split(/\0/, $in{'d'});
my $start = 1 if $in{'start'};
my $shutdown = 1 if $in{'shutdown'};
my $suspend = 1 if $in{'suspend'};
my $resume = 1 if $in{'resume'};
my $destroy = 1 if $in{'destroy'};

&ui_print_unbuffered_header(undef, $start ? $text{'mass_start'} : 
			$shutdown ? $text{'mass_shutdown'} : 
			$suspend ? $text{'mass_suspend'} : 
			$resume ? $text{'mass_resume'} : 
			$text{'mass_stop'}, "");

if ($start) {
	foreach my $dom (@sel) {
		print &text('ss_start', "<tt>$dom</tt>");
        print "<pre>";
        print &start_domain($dom)."<p>\n";
        print "</pre>";
	}
} elsif($suspend) {
	foreach my $dom (@sel) {
		print &text('ss_suspend', "<tt>$dom</tt>");
        print "<pre>";
        print &suspend_domain($dom)."<p>\n";
        print "</pre>";
	}
} elsif($resume){
	foreach my $dom (@sel) {
		print &text('ss_resume', "<tt>$dom</tt>");
        print "<pre>";
        print &resume_domain($dom)."<p>\n";
        print "</pre>";
	}
} else {
	foreach my $dom (@sel) {
		print &text('ss_stop', "<tt>$dom</tt>");
        print "<pre>";
        if ($destroy) {
        	print &destroy_domain($dom)."<p>\n";
        } else {
        	print &shutdown_domain($dom)."<p>\n";
        }
        print "</pre>";
	}
}

&ui_print_footer("", $text{'index_return'});
