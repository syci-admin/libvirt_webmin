#!/usr/bin/perl

require 'virt-manager-lib.pl';

&ReadParse();

my @sel = split(/\0/, $in{'d'});
my $displayoff = get_vncdisplay($sel[0]);
my $displayname = get_domainname($sel[0]);

&ui_print_unbuffered_header(undef, "Viewer for $displayname");

system("/usr/libexec/webmin/virt-manager/redirect-socket.pl -p ".$sel[0]." &");

print "<APPLET CODE=\"VncViewer.class\" ARCHIVE=\"VncViewer.jar\" WIDTH=\"1024\" HEIGHT=\"800\">";
print "<PARAM NAME=\"PORT\" VALUE=\"".(5900 + $displayoff)."\">";
print "</APPLET>";
print "<BR>";

&ui_print_footer("", $text{'index_return'});

