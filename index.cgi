#!/usr/bin/perl


require 'virt-manager-lib.pl';


$refresh = "<a href=/virt-manager>$text{'virt_refresh'}</a>";
ui_print_header(undef, $text{'index_title'}, "", undef, 1, 1, 0, $refresh);

my @doms = &get_domains();

print &ui_form_start("mass_start_stop.cgi", "post");

print &ui_columns_start([
	"",
	$text{'index_id'},
	$text{'index_name'},
	$text{'index_vcpu'},
	$text{'index_vmemory'},
	$text{'index_status'},
	$text{'index_view'} ],
	100);

foreach my $dom (@doms) {
	print &ui_columns_row([
		$$dom{'id'} eq "0" ? "": &ui_checkbox("d", $$dom{'name'}, undef),
		$$dom{'id'},
		$$dom{'name'}, 
		$$dom{'cpus'},
		$$dom{'memory'},
		"<img src=icons/state_".(($$dom{'status'} =~ s/\s+//), $$dom{'status'} =~ m/nostate/ || $$dom{'status'} =~ m/blocked/ ? "running" : $$dom{'status'}).".png title=".$$dom{'status'}." width=24px>",
		($access{'virtsee'} ? (($$dom{'id'} eq "0" || $$dom{'id'} eq "-") ? "" : "<a href=view.cgi?d=".$$dom{'id'}."><img src=icons/nohost.png width=24px></a>") : "")
	]);
}

print &ui_columns_end();
if ($access{'virtreboot'}) {
print &ui_form_end([
	["start", $text{'button_action_start'}],
	["shutdown", $text{'button_action_shutdown'}],
	["destroy", $text{'button_action_destroy'}],
	["suspend", $text{'button_action_suspend'}],
	["resume", $text{'button_action_resume'}]
	]);
}
print &ui_hr();
&ui_print_footer("/", $text{'index'});

