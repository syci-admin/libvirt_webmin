#!/usr/bin/perl

use strict;
use IO::Socket;
use IO::Select;
use Getopt::Std;
my ($sock_select,$rec_sock,$dest_sock,$listen_sock);
my %opts;
getopt('p:',\%opts);
my $port = $opts{'p'};

$listen_sock = new IO::Socket::INET(Listen => 5, LocalPort => 30000+$port, Proto => 'tcp');

if (!$listen_sock) { die "Error listening on port ".(30000 + $port)."\n"; }

$sock_select = new IO::Select();

while(1) {
	if(!$rec_sock) {
		#Waiting for connection
		$rec_sock = $listen_sock->accept;
		#Once connection is accepted, adding the connection to the select
		$sock_select->add($rec_sock);
	} elsif($rec_sock && !$dest_sock) {
		#We have the client peer socket, let's connect on the virtual machine
		$dest_sock = IO::Socket::INET->new(PeerAddr=>'localhost:59'.$port, proto=>'tcp');
		if(!$dest_sock) {
			die "Error listening on port localhost";
		}
		#Add the connection to the select
		$sock_select->add($dest_sock);
	} else {
		my ($bufflen,$buff);
		#We have both connections, server side and client side
		my @ready_socks = $sock_select->can_read();
		foreach my $sock (@ready_socks) {
			$bufflen = sysread($sock,$buff,4096); 
			if($bufflen == 0) {
				#End of connection, killing all sessions
				$dest_sock->close();
				$rec_sock->close();
				$listen_sock->close();
				exit 0;
			}
			if($sock eq $dest_sock) {
				syswrite($rec_sock,$buff);
			} else {
				syswrite($dest_sock,$buff);
			}
		}
	}
}

